import x10.util.Timer;
import x10.io.File;
import x10.io.Reader;
import x10.io.FileWriter;
import x10.io.FileReader;
import x10.io.BufferedReader;
import x10.lang.Exception;
import x10.compiler.Inline;
import org.scalegraph.util.MemoryChunk;

public class Graph {
    val nb_nodes: int;
    val nb_links: int;
    
    val degrees: MemoryChunk[int];
    val links: MemoryChunk[int];
    val e2c: MemoryChunk[int]; //store cluster info of each edge

    val filename: String;

    public def this(filename: String, clusterNum: int) {
        //read from bin file
        val finput = new File(filename);
        val freader = new BufferedReader(new FileReader(finput));

        nb_nodes = freader.readInt();

        degrees = MemoryChunk.make[int](nb_nodes+1);
        degrees(0) = 0;
        for (i in 1 .. nb_nodes) {
            degrees(i) = freader.readInt();
        }
        
        nb_links = degrees(nb_nodes);
        e2c = MemoryChunk.make[int](nb_links, (i :long) => -1);
        links = MemoryChunk.make[int](nb_links);
        for (i in 0..(nb_links-1)) {
            links(i) = freader.readInt();
        }

        this.filename = finput.getName(); 
    }

    public static def main(args: Array[String]) {
        val timer = new Timer();
        var start: long = timer.milliTime();
        var fileName: String = args(0);
        val finput = new File(fileName);
        val freader = finput.openRead();
        for ( line in finput.lines()) {
            
        }
        var end: long = timer.milliTime();
        Console.OUT.println("It used "+(end-start)+"ms");
    }
}
