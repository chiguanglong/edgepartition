import x10.util.*;
import x10.lang.*;
import x10.io.File;
import x10.io.FileWriter;
import x10.io.Printer;

public class ProfileWriter {

	private val file: File;
	private val printer: Printer;

	def this(filename: String) {
		file = new File(filename);
		printer = file.printer();
	}

	def this(filename: String, items: Array[String]) {
		file = new File(filename);
		printer = file.printer();
		val size = items.size - 1;
		for (i in 0..size) {
			if (i!=size) 
				printer.print(items(i));
			else 
				printer.print(items(i) + ",");
		}
		printer.println();
	}

	public def setItems(items: Array[String]) {
		val size = items.size - 1;
		for (i in 0..size) {
			if (i!=size) 
				printer.print(items(i));
			else 
				printer.print(items(i) + ",");
		}
		printer.println();
	}

	public def write(line: String) {
		printer.print(line);
	}

	public def writeln(line: String) {
		printer.println(line);
	}

	public def write(line: Any) {
		printer.print(line);
	}

	public def writeln(line: Any) {
		printer.println(line);
	}

	public def writeInt(line: int) {
		printer.writeInt(line);
	}

	public def writeDouble(line: double) {
		printer.writeDouble(line);
	}

	public def flush() {
		printer.flush();
	}

	public def close() {
		printer.close();
	}
}
