import x10.lang.*;
import x10.array.Point;
import x10.util.Random;
import x10.util.Timer;
import x10.io.File;
import x10.io.FileReader;
import x10.compiler.Inline;
import org.scalegraph.util.MemoryChunk;
import org.scalegraph.util.GrowableMemory;

public class EdgePartition {
	val edgeSize: MemoryChunk[int]; //store the number of edges in each cluster
	val masterCluster: MemoryChunk[int]; 

	val nodeAllocation: MemoryChunk[MemoryChunk[ULong]]; // nodeAllocation(place, node) = 1/0

	val g: Graph;
	val placeNum: int;
	val rnd: Random = new Random();

	def this(placeNum: int, filename: String) {
	    g = new Graph(filename, placeNum);
	  	edgeSize = MemoryChunk.make[int](placeNum, 0);
	  	
	  	val size = ((g.nb_nodes + 16 * 4 - 1) / (16 * 4));
	  	nodeAllocation = MemoryChunk.make[MemoryChunk[ULong]](placeNum, (i: long) => MemoryChunk.make[ULong](size, 0l));

	    this.placeNum = placeNum;
	    masterCluster = MemoryChunk.make[int](g.nb_nodes, 0);
	}

	@Inline public def clearEdgeSize( ) {
		//clear edgeSize 
		for (i in edgeSize.range()) edgeSize(i) = 0;
	}

	@Inline public def sizeAdjustFunction(size: int) {
	  	return (-1) * Math.sqrt(size)*100;
	}

	@Inline public def pairingFunction(node: double, neigh: double) {
		return 1./2. * (node+neigh) * (node+neigh+1.) + neigh;
	}

	public def partition(showDetail: boolean) {
	  	val random_order = MemoryChunk.make[int](g.nb_nodes, (i: long) => i as int);

	    for (i in 0 .. (g.nb_nodes-1)) {
			val rand_pos = rnd.nextInt(g.nb_nodes);
			val tmp = random_order(i);
			random_order(i) = random_order(rand_pos);
			random_order(rand_pos) = tmp;
	    }

	    Console.OUT.println("start partition");
	    Console.OUT.flush();
	    
	    var maxCluster: int = -1;

	    for (i in 0 .. (g.nb_nodes-1)) {
	    	val n1 = random_order(i);
	      	for (linkIndex in g.degrees(n1) .. (g.degrees(n1+1)-1) ) {
	        	val n2 = g.links(linkIndex);
	        	if (n1 > n2) continue;

	        	maxCluster = Int.MIN_VALUE;
	        	var maxProductNodeNum: ULong = 0l;  //compute (#product nodes) between two cluster, value = 0, 1, 2
	        	var maxValue: double = Double.NEGATIVE_INFINITY;

	        	val bitpos1 = (n1 % 64) - 1; 
	        	val bitpos2 = (n2 % 64) - 1;
	        	for (place in 0 .. (nodeAllocation.size() - 1) as int) {
	        		val na = nodeAllocation(place);
	        		val x = (na(n1 / 64) & (1ul << bitpos1))  == 0ul ? 0ul : 1ul;
	        		val y = (na(n2 / 64) & (1ul << bitpos2)) == 0ul ? 0ul : 1ul;
	        		val productNodeNum = x + y; //productNodeNum: 0, 1, 2

	        		if (productNodeNum < maxProductNodeNum) continue;

		        	val value = productNodeNum * sizeAdjustFunction(edgeSize(place));

	        		if (productNodeNum > maxProductNodeNum) {
	        			maxProductNodeNum = productNodeNum;
	        			maxCluster = place;
	        			maxValue = value;
	        			continue;
	        		}

		        	if (value > maxValue) {
		        		maxValue = value;
		        		maxCluster = place;
		        	}
	        	}

	        	if (maxProductNodeNum == 0ul) maxCluster = rnd.nextInt(placeNum);
	        	val na = nodeAllocation(maxCluster);
	        	na(n1 / 64) |= (1ul << bitpos1);
	        	na(n2 / 64) |= (1ul << bitpos2);
	        	masterCluster(n1) = masterCluster(n2) = maxCluster;
		        g.e2c(linkIndex)  = maxCluster; //set location of each edge
				edgeSize(maxCluster) += 1;
	    	}		
	    }

	    if(showDetail) {
	    	//count edge number
			for(i in edgeSize.range()){
				val nodeSize = getNodeSize(i);
				
				Console.OUT.println("#edges = " + edgeSize(i) + 
					", #nodes = " + nodeSize + ", place = " + i);

	    		edgeSize(i) = nodeSize; //store the size of nodes per place for writing partitioned file in writeBinaryGraph()
			}
	    }
	}

	public def getNodeSize(place: long) {
		var sum: int = 0;
		for (i in nodeAllocation(place).range()) sum += nodeAllocation(place)(i).bitCount();
		return sum;
	}

	public def getCommunicateCostPerPlace() {
		clearEdgeSize(); //store the number of borders in each place
		for (place in 0..(placeNum-1)) {
			val bitmaps = nodeAllocation(place);
			for (i in (0 .. (g.nb_nodes - 1))) {
				val bitmap = bitmaps(i / 64);
				val bitpos = (i % 64) - 1;
				if ((bitmap & (1ul << bitpos)) > 0u && masterCluster(i) != place) edgeSize(place) += 1;
			}
		}

		var sum: int = 0;
		for (i in edgeSize.range()) {
			Console.OUT.println("borders" + i + " = " + edgeSize(i));
			sum += edgeSize(i);
		}
		Console.OUT.println("EdgePartition, communication cost with aggregation = " + sum);
	}

	public def masterNodePerPlace() {
		clearEdgeSize();  //count the number of master nodes

		for (node in 0..(g.nb_nodes-1)) {
			if(masterCluster(node)<0) continue;
			edgeSize(masterCluster(node)) += 1;
		}

		for (i in edgeSize.range()) {
			Console.OUT.println("master nodes = " + edgeSize(i) + ", place = " + i);
		}
	}

	@Inline public def getNeighPlace(node: int, neigh: int) {
		val start = g.degrees(neigh);
		val end = g.degrees(neigh+1) - 1;
			
		for(linkIndex in start..end) {
			val neighNode = g.links(linkIndex);
			if(neighNode==node) return g.e2c(linkIndex);
		}
		return -1;
	}

	public def writeBinaryGraph() {
	//nb_nodes, #nodesPerPlace, totalWeight, 
	//<nodeList(i), degree(i), degree_part(i), masterP>, <links>
		Console.OUT.println("start write binary graph!!");

		val filenameIndex = g.filename.lastIndexOf(".bin");
    	val filename = g.filename.substring(0, filenameIndex);
    	val profile = MemoryChunk.make[ProfileWriter](placeNum);
    	val placeLinks = MemoryChunk.make[GrowableMemory[int]](placeNum); //store linklist of each place

    	//init file
    	for (place in 0..(placeNum-1)) {
    		profile(place) = new ProfileWriter(filename+"-"+place+".bin");
    		placeLinks(place) =  new GrowableMemory[int]();
    		
    		profile(place).writeInt(g.nb_nodes);
      		profile(place).writeInt(edgeSize(place)); //#NodesPerPlace
      		profile(place).writeDouble(g.nb_links);
    	}

    	Console.OUT.println("Compute link size!");

    	for(node in 0..(g.nb_nodes-1)) {
    		val start = g.degrees(node);
    		val end = g.degrees(node+1) - 1;
    		clearEdgeSize();  //store the number of links in each place

    		for (linkIndex in start..end) {
    			val neigh = g.links(linkIndex);
				val neighPlace = node>neigh ? getNeighPlace(node, neigh) : g.e2c(linkIndex);

				edgeSize(neighPlace) = 1;
				placeLinks(neighPlace).add(neigh);
    		}

    		for (place in edgeSize.range()) {
    			if(edgeSize(place)==0) continue;

    			profile(place).writeInt(node);
          		profile(place).writeInt(g.degrees(node+1)-g.degrees(node));
          		profile(place).writeInt(placeLinks(place).size() as int);
          		profile(place).writeInt(masterCluster(node));
    		}
    	}

    	Console.OUT.println("Write links!");

    	//write links and close file
    	for (place in 0..(placeNum-1)) {
    		for(i in placeLinks(place).range()) {
    			profile(place).writeInt(placeLinks(place)(i));
    		}
    		profile(place).close();
    	}

    	profile.del();
    	placeLinks.del();
    	g.degrees.del();
    	g.links.del();
    	g.e2c.del();
	} 

	public def writeNodeDegree() {
	//nb_nodes, #nodesPerPlace, totalWeight, 
	//<nodeList(i), degree(i), degree_part(i), masterP>, <links>
		Console.OUT.println("start write graph degree!!");

		val filenameIndex = g.filename.lastIndexOf(".bin");
    	val filename = g.filename.substring(0, filenameIndex);
    	
    	val profileDegree = new ProfileWriter(filename + "-Degree.csv");
    	profileDegree.writeln("node, degree");
    	
    	val profileDegreeP = MemoryChunk.make[ProfileWriter](placeNum);
    	val placeLinks = MemoryChunk.make[GrowableMemory[int]](placeNum); //store linklist of each place

    	//init file
    	for (place in 0..(placeNum-1)) {
    		profileDegreeP(place) = new ProfileWriter(filename+"-"+place+"-nodeDegree.csv");
    		placeLinks(place) =  new GrowableMemory[int]();
    	}

    	Console.OUT.println("Compute link size!");
    	for(node in 0..(g.nb_nodes-1)) {
    		val start = g.degrees(node);
    		val end = g.degrees(node+1) - 1;
    		clearEdgeSize(); //store the number of links in each place
    		
    		profileDegree.write(node);
    		profileDegree.write(",");
    		profileDegree.writeln(end-start+1);

    		for (linkIndex in start..end) {
    			val neigh = g.links(linkIndex);
				val neighPlace = node>neigh ? getNeighPlace(node, neigh) : g.e2c(linkIndex);

				edgeSize(neighPlace) = 1;
				placeLinks(neighPlace).add(neigh);
    		}

    		for (place in edgeSize.range()) {
    			if(edgeSize(place)==0) continue;
    			val degree  = g.degrees(node+1)-g.degrees(node);
    			val degreeP = placeLinks(place).size() as int;

    			profileDegreeP(place).write(node);
    			profileDegreeP(place).write(",");
          		profileDegreeP(place).write(degree);
    			profileDegreeP(place).write(",");
          		profileDegreeP(place).write(degreeP);
    			profileDegreeP(place).write(",");
          		profileDegreeP(place).write(masterCluster(node));
    			profileDegreeP(place).write(",");
          		
          		val ifPartitioned = (degree!=degreeP) ? 1 : 0;//"yes" : "no";
          		profileDegreeP(place).writeln(ifPartitioned);
    		}
    	}
    	profileDegree.close();

    	Console.OUT.println("Write links!");
    	//close file
    	for (place in 0..(placeNum-1)) {
    		profileDegreeP(place).close();
    	}

    	profileDegreeP.del();
    	placeLinks.del();
	}
}
