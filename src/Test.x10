import x10.util.*;
import x10.lang.*;
import x10.io.File;
import x10.io.FileWriter;
import x10.io.Printer;
import x10.util.HashSet;
import x10.compiler.Inline;
import org.scalegraph.util.MemoryChunk;

public class Test {

	@Inline public def pairingFunction(node: double, neigh: double) {
		var value:double;
		value = (node+neigh) * (node+neigh+1.) / 2.; 
		value += neigh;
		return value;
	}

	static class Sum {
		var a:int;
		var b:int;
		def this (a: int, b: int){
			this.a = a;
			this.b = b;
		}
		public def sum( ) ={
			return a+b;
		};
	}

	protected static class TestA {
		var a:int;
		var b:int;
		def this (a: int, b: int){
			this.a = a;
			this.b = b;
		}
		public def getSum() {
			val testSum = new Sum(a, b);
			return testSum.sum();
		}
	}

	public def show() {
		val testA = new TestA(10, 15);
		Console.OUT.println(testA.getSum());
	}
	public static def main(args: Array[String]) {   
		// var test:int = 8;
		// test <<=2;
		// Console.OUT.println("test: " + test);

		// var test1:Test = new Test();
		// test1.show();
		// var node:int = Int.parseInt(args(0));
		// var neigh:int = Int.parseInt(args(1));

		// var value:double = test1.pairingFunction(node, neigh);
		// Console.OUT.println("Value = " + value);
		val k: ULong = 1ul;
		val a = MemoryChunk.make[long](Long.parseLong(args(0)), (i:long)=>i);
		val s1 = Timer.milliTime();
		val b = a.toArray();
		Console.OUT.println("sum = " + (b.reduce((sum:long, i:long)=>sum+i, 0l)));
		val s2 = Timer.milliTime();
		var sum:long = 0l;
		for( i in a.range()) sum +=a(i);
		Console.OUT.println("sum = " + (sum));
		Console.OUT.println("time1: " + (s2-s1)+ "ms, time2: " + (Timer.milliTime()-s2)+ "ms");		
	}
}