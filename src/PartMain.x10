import x10.util.*;
import x10.lang.*;
import x10.io.File;
import x10.io.FileWriter;
import x10.io.Printer;

public class PartMain {
	var clusterNum: int;
	var filename: String;
	var parFilename: String = null;
	var showDetail: boolean = false;
	var showBordersPerPlace: boolean = false;
	var writeDegree: boolean = false;
	var writeFile: boolean = false;

    public def parse_args(args: Array[String]) {
    	val opts = new OptionsParser(args, [
    		Option("h", "help", "show help information"),
    		Option("d","detail","show detail info of edge partition"),
    		Option("b", "borders", "show replicas of each place"),
    		Option("D", "degree", "write degree profile"),
    		Option("P", "parfile", "write partitioned graph file")
    		], [
    		Option("i", "input", "input filename"),
    		Option("p", "part", "specify the number of partition"),
    		Option("f", "pfile", "input prepartitioned filename")
    		]);

    	if(opts.filteredArgs().size!=0){
    		Console.ERR.println("Unexpected arguments: "+opts.filteredArgs());
            Console.ERR.println("Use -h or --help.");
            return false;
    	}

    	if (opts("-h")) {
            Console.OUT.println(opts.usage());
            return false;
        }

        showDetail = opts("-d");
        showBordersPerPlace = opts("-b");
        writeDegree = opts("-D");
        writeFile = opts("-P");

        clusterNum = opts("-p", 2);
        filename = opts("-i", null);
        parFilename = opts("-f", null);
        return true;
    }

    public static def main(args: Array[String]) {   
        val timer = new Timer();
        val startTime = timer.milliTime();
        val startMem = Memory.getGCMemSize();

        val partMain = new PartMain();
        if(!partMain.parse_args(args)) return;

        val edgePartition = new EdgePartition(partMain.clusterNum, partMain.filename);
        edgePartition.partition(partMain.showDetail);
        
        if(partMain.writeDegree)
          edgePartition.writeNodeDegree();
        
        if(partMain.writeFile)
          edgePartition.writeBinaryGraph();  
              
        edgePartition.masterNodePerPlace();

        if(partMain.showBordersPerPlace){
          edgePartition.getCommunicateCostPerPlace();
        }

        val endTime = timer.milliTime();
        Console.OUT.println("Time: " + (endTime - startTime) + "ms");

        val endMem = Memory.getGCMemSize();
        Console.OUT.println("Memory: " + (endMem - startMem)/(1024*1024)+"MB");
    }
}
