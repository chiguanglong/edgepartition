import x10.compiler.Native;
import x10.lang.Int;

public class Memory {
	@Native("c++", "GC_get_heap_size()")
	public static native def getGCMemSize(): long;

	@Native("c++", "GC_INIT()")
	public static native def initGC(): void;
}
