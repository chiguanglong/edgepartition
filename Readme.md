This program uses the same format of graph data as ParallelLouvain Method.

Do edge partition with a graph data
	- ./bin/edgePartition -i inputfile -p theNumberOfPartitions -pFile thePartitionedInfoFile
	ex.	
		1.When doing naive edge partition
		- ./bin/edgePartition -i emailiso.bin -p 8 

		2.When showing the communication cost of metis.
		- ./bin/edgePartition -i emailiso8.bin -p 8 -pFile emailiso.graph.part.8
options
	-d: show #edges, #nodes per place under edge partition
	-bordersP: show borders of each place

