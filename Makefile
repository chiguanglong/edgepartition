#!/bin/bash

CC=x10c++
EXEC=edgePar

CFLAGS=-J-Xmx2G -cxx-prearg  -I/home/usr5/14D55021/lib/scalegraph-2.2/include -sourcepath /home/usr5/14D55021/lib/scalegraph-2.2/src
FLAGS=-OPTIMIZE=true -VERBOSE  -O -NO_CHECKS -define NO_BOUNDS_CHECKS  -cxx-prearg -g  

all: $(EXEC)
edgePar : src/PartMain.x10
	$(CC) $(FLAGS) -o bin/$@ $^ 
	mv *.h obj/
	mv *cc obj/
install : all
	mv *.h obj/
	mv *cc obj/
	
##########################################
# Generic rules
##########################################

clean:
	rm -f *.cc *~ *.h bin/$(EXEC) obj/*h obj/*cc
